package ru.tsc.panteleev.tm.api.service;

import ru.tsc.panteleev.tm.api.model.ICommand;
import ru.tsc.panteleev.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command);

    Collection<AbstractCommand> getTerminalCommands();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

}
