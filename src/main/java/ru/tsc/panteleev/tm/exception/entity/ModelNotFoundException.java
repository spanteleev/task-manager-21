package ru.tsc.panteleev.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Record not found...");
    }
}
