package ru.tsc.panteleev.tm.command.system;

import ru.tsc.panteleev.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractSystemCommand {

    public static final String NAME = "help";

    public static final String DESCRIPTION = "Show terminal commands.";

    public static final String ARGUMENT = "-h";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command);
        }
    }

}
